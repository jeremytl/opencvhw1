
// MFCApplication1Dlg.cpp : 實作檔
//

#include "stdafx.h"
#include "MFCApplication1.h"
#include "MFCApplication1Dlg.h"
#include "afxdialogex.h"
#include "opencv2\highgui\highgui.hpp"
#include "opencv\cv.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 對 App About 使用 CAboutDlg 對話方塊

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 對話方塊資料
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支援

	// 程式碼實作
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCApplication1Dlg 對話方塊



CMFCApplication1Dlg::CMFCApplication1Dlg(CWnd* pParent /*=NULL*/)
: CDialogEx(CMFCApplication1Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCApplication1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMFCApplication1Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CMFCApplication1Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON3, &CMFCApplication1Dlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON2, &CMFCApplication1Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON6, &CMFCApplication1Dlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON4, &CMFCApplication1Dlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CMFCApplication1Dlg::OnBnClickedButton5)
END_MESSAGE_MAP()


// CMFCApplication1Dlg 訊息處理常式

BOOL CMFCApplication1Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 將 [關於...] 功能表加入系統功能表。

	// IDM_ABOUTBOX 必須在系統命令範圍之中。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 設定此對話方塊的圖示。當應用程式的主視窗不是對話方塊時，
	// 框架會自動從事此作業
	SetIcon(m_hIcon, TRUE);			// 設定大圖示
	SetIcon(m_hIcon, FALSE);		// 設定小圖示

	// TODO:  在此加入額外的初始設定

	return TRUE;  // 傳回 TRUE，除非您對控制項設定焦點
}

void CMFCApplication1Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果將最小化按鈕加入您的對話方塊，您需要下列的程式碼，
// 以便繪製圖示。對於使用文件/檢視模式的 MFC 應用程式，
// 框架會自動完成此作業。

void CMFCApplication1Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 繪製的裝置內容

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 將圖示置中於用戶端矩形
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 描繪圖示
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 當使用者拖曳最小化視窗時，
// 系統呼叫這個功能取得游標顯示。
HCURSOR CMFCApplication1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCApplication1Dlg::OnBnClickedButton1()
{
	// TODO:  在此加入控制項告知處理常式程式碼

	IplImage* img = cvLoadImage("./OpenCv_Hw1_20151016.jpg");
	cvNamedWindow("Example1", CV_WINDOW_AUTOSIZE);
	cvShowImage("Example1", img);
	cvWaitKey(0);
	cvReleaseImage(&img);
	cvDestroyWindow("Example1");


}

CvRect box, drag, dragline;
bool drawing_box = false;
void draw_box(IplImage* img, CvRect rect) {
	cvRectangle(
		img,
		cvPoint(box.x, box.y),
		cvPoint(box.x + box.width, box.y + box.height),
		cvScalar(0xff, 0x00, 0x00) /* red */
		);
}
// Define our callback which we will install for
// mouse events.
//

void my_mouse_callback(
	int event, int x, int y, int flags, void* param
	)
{
	IplImage* image = (IplImage*)param;
	switch (event) {
	case CV_EVENT_MOUSEMOVE: {
								 if (drawing_box) {
									 box.width = x - box.x;
									 box.height = y - box.y;
								 }
	}
		break;
	case CV_EVENT_LBUTTONDOWN: {
								   drawing_box = true;
								   box = cvRect(x, y, 0, 0);
	}
		break;
	case CV_EVENT_LBUTTONUP: {
								 drawing_box = false;
								 if (box.width<0) {
									 box.x += box.width;
									 box.width *= -1;
								 }
								 if (box.height<0) {
									 box.y += box.height;
									 box.height *= -1;
								 }
								 //draw_box(image, box);
	}
		break;
	}
}




void CMFCApplication1Dlg::OnBnClickedButton3()   // 1.2
{
	// TODO:  在此加入控制項告知處理常式程式碼
	box = cvRect(-1, -1, 0, 0);
	IplImage* image = cvLoadImage("./OpenCv_Hw1_20151016.jpg");

	IplImage* temp = cvCloneImage(image);

	cvNamedWindow("Box");
	// Here is the crucial moment that we actually install
	// the callback. Note that we set the value ‘param’ to
	// be the image we are working with so that the callback
	// will have the image to edit.
	//
	cvSetMouseCallback(
		"Box",
		my_mouse_callback,
		(void*)image
		);
	// The main program loop. Here we copy the working image
	// to the ‘temp’ image, and if the user is drawing, then
	// put the currently contemplated box onto that temp image.
	// display the temp image, and wait 15ms for a keystroke,
	// then repeat…
	//
	while (1) {

		if (drawing_box){
			cvCopyImage(image, temp);
			draw_box(temp, box);
		}

		cvShowImage("Box", temp);
		if (cvWaitKey(15) == 27) break;
	}
	// Be tidy
	//
	cvReleaseImage(&image);
	cvReleaseImage(&temp);
	cvDestroyWindow("Box");
}



void CMFCApplication1Dlg::OnBnClickedButton2()  //  1.3
{
	// TODO:  在此加入控制項告知處理常式程式碼
	IplImage* image = cvLoadImage("./OpenCv_Hw1_20151016.jpg");
	cvSetImageROI(image, box);
	cvNamedWindow("InTheBox");
	cvShowImage("InTheBox", image);
	cvWaitKey(0);
	cvReleaseImage(&image);
	cvDestroyWindow("Box");
}


void CMFCApplication1Dlg::OnBnClickedButton6()//2.1
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CvCapture* src = cvCreateFileCapture("./OpenCv_Hw1_20151016.avi");
	IplImage* frame = cvQueryFrame(src);
	cvNamedWindow("2.1", CV_WINDOW_AUTOSIZE);
	double f = cvGetCaptureProperty(src, 5);//get FPS of the video

	while (1)
	{
		cvShowImage("2.1", frame);
		frame = cvQueryFrame(src);
		if (cvWaitKey(1000 / f) == 27) break;
	}

	cvReleaseImage(&frame);
	cvReleaseCapture(&src);
	cvDestroyWindow("2.1");

}


void CMFCApplication1Dlg::OnBnClickedButton4()//2.2
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CvCapture* src = cvCreateFileCapture("./OpenCv_Hw1_20151016.avi");
	IplImage* frame = cvQueryFrame(src);
	cvNamedWindow("2.2", CV_WINDOW_AUTOSIZE);
	double f = cvGetCaptureProperty(src, 5);//get FPS of the video
	double fcnt = cvGetCaptureProperty(src, CV_CAP_PROP_POS_FRAMES);
	int t = 0;
	char text[7];
	strcpy(text, "00:00");
	CvFont* font = &cvFont(2, 0);

	while (1)
	{
		cvShowImage("2.2", frame);

		fcnt = cvGetCaptureProperty(src, CV_CAP_PROP_POS_FRAMES);
		t = (int)(fcnt / f);



		text[4] = (char)('0' + t);
		frame = cvQueryFrame(src);
		cvPutText(
			frame,
			text,
			cvPoint(cvGetCaptureProperty(src, CV_CAP_PROP_FRAME_WIDTH) - 100, cvGetCaptureProperty(src, CV_CAP_PROP_FRAME_HEIGHT) - 15),
			font,
			cvScalar(0x00, 0xFF, 0x00)/*GREEN!!*/
			);
		if (cvWaitKey(800 / f) == 27) break;

	}
	cvReleaseImage(&frame);
	cvReleaseCapture(&src);
	cvDestroyWindow("2.2");
}


void CMFCApplication1Dlg::OnBnClickedButton5()//2.3
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CvCapture* src = cvCreateFileCapture("./OpenCv_Hw1_20151016.avi");
	IplImage* frame = cvQueryFrame(src);
	cvNamedWindow("2.3", CV_WINDOW_AUTOSIZE);
	double f = cvGetCaptureProperty(src, 5);//get FPS of the video
	double fcnt = cvGetCaptureProperty(src, CV_CAP_PROP_POS_FRAMES);

	int slide = 0;
	cvCreateTrackbar("slider", "2.3", &slide, 5);


	int t = 0;
	char text[7];
	strcpy(text, "00:00");
	CvFont* font = &cvFont(2, 0);

	while (1)
	{
		cvShowImage("2.3", frame);

		fcnt = cvGetCaptureProperty(src, CV_CAP_PROP_POS_FRAMES);
		t = (int)(fcnt / f);


		cvSetTrackbarPos("slider", "2.3", t);
		text[4] = (char)('0' + t);
		frame = cvQueryFrame(src);
		cvPutText(
			frame,
			text,
			cvPoint(cvGetCaptureProperty(src, CV_CAP_PROP_FRAME_WIDTH) - 100, cvGetCaptureProperty(src, CV_CAP_PROP_FRAME_HEIGHT) - 15),
			font,
			cvScalar(0x00, 0xFF, 0x00)/*GREEN!!*/
			);
		if (cvWaitKey(800 / f) == 27) break;

	}

	cvReleaseImage(&frame);
	cvReleaseCapture(&src);
	cvDestroyWindow("2.3");
}
